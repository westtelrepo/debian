#!/usr/bin/lua

freeswitch.consoleLog("info","Get Memory usage v1.0\n")
-- Version 1.0
-- run from the cli luarun mem.usage.lua



function os.capture(cmd, raw)
  local f = assert(io.popen(cmd, 'r'))
  local s = assert(f:read('*a'))
  f:close()
  if raw then return s end
  s = string.gsub(s, '^%s+', '')
  s = string.gsub(s, '%s+$', '')
  s = string.gsub(s, '[\n\r]+', ' ')
  return s
end

local function lines(str)
  local result = {}
  for line in str:gmatch '[^\n]+' do
    table.insert(result, line)
  end
  return result
end

-- find OS Memory Usage ..............................................

ret = os.capture("cat /proc/meminfo", 0);
if (ret==nil) then ret="" end
if (ret=="") then
 freeswitch.consoleLog("crit","Freeswitch Memory Usage: No Data returned!\n");
 return
end

memtable = {}

memtable = lines(ret)

 for i, line in ipairs(memtable) do
  if string.find(line,"MemTotal:") then
   memtotal = line:gsub("MemTotal:","")
   memtotal = memtotal:gsub("kB","")
   memtotal = memtotal:gsub(" ","")
   freeswitch.consoleLog("info","OS Memory Total: "..memtotal.." kB\n");
  end;

  if string.find(line,"MemAvailable:") then
   memavail = line:gsub("MemAvailable:","")
   memavail = memavail:gsub("kB","")
   memavail = memavail:gsub(" ","")
   freeswitch.consoleLog("info","OS Memory Avail: "..memavail.." kB\n");
  end;

  if string.find(line,"SwapTotal:") then
   swaptotal = line:gsub("SwapTotal:","")
   swaptotal = swaptotal:gsub("kB","")
   swaptotal = swaptotal:gsub(" ","")
   freeswitch.consoleLog("info","OS Swap Total: "..swaptotal.." kB\n");
  end;

  if string.find(line,"SwapFree:") then
   swapfree = line:gsub("SwapFree:","")
   swapfree = swapfree:gsub("kB","")
   swapfree = swapfree:gsub(" ","")
   freeswitch.consoleLog("info","OS Swap Free: "..swapfree.." kB\n");
  end;


 end;
 
 if (memtotal==nil) then memtotal="" end
 if (memtotal=="") then 
  freeswitch.consoleLog("crit","OS Memory Total: No Data returned!\n");
  return
 end
 if (memavail==nil) then memavail="" end
 if (memavail=="") then 
  freeswitch.consoleLog("crit","OS Memory Avail: No Data returned!\n");
  return
 end

 totalpct = (memtotal-memavail)/memtotal*100
 totalpct = math.floor(totalpct)
 freeswitch.consoleLog("info","OS Memory Use: "..totalpct.." %\n");

 if (swaptotal==nil) then swaptotal="" end
 if (swaptotal=="") then 
  freeswitch.consoleLog("crit","OS Swap Total: No Data returned!\n");
  return
 end
 if (swapfree==nil) then swapfree="" end
 if (swapfree=="") then 
  freeswitch.consoleLog("crit","OS Swap Free: No Data returned!\n");
  return
 end

 swappct = (swaptotal-swapfree)/swaptotal*100
 swappct = math.floor(swappct )
 freeswitch.consoleLog("info","OS Swap Use: "..swappct .." %\n");


-- find freeswitch memory usage ............................................

ret = os.capture("pidof freeswitch", 0);
if (ret==nil) then ret="" end
if (ret=="") then
 freeswitch.consoleLog("crit","Freeswitch Memory Usage: No Pid of freeswitch returned!\n");
 return
end

PID = math.floor(ret)

freeswitch.consoleLog("info","Freeswitch Pid: "..PID.."\n");

ret = os.capture("top -b -n 1 -p "..PID, 0);
if (ret==nil) then ret="" end
if (ret=="") then
 freeswitch.consoleLog("crit","Freeswitch Memory Usage: No ps up for freeswitch returned!\n");
 return
end

chunks = {}
for substring in ret:gmatch("%S+") do
   table.insert(chunks, substring)
end

 PIDint = math.floor(PID)

 for i, line in ipairs(chunks) do
  if string.find(line,PIDint) then
   fswcpupercent=chunks[i+8]
   fswmempercent=chunks[i+9]
   freeswitch.consoleLog("info","Freeswitch CPU Usage: "..fswcpupercent.." %\n");
   freeswitch.consoleLog("info","Freeswitch MEM Usage: "..fswmempercent.." %\n");
  end 

 end;

-- fire event to event socket

local event = freeswitch.Event("CUSTOM", "myevent::memory_usage");
event:addHeader("MEM_TOTAL",  memtotal);
event:addHeader("MEM_AVAIL",  memavail);
event:addHeader("SWAP_TOTAL", swaptotal);
event:addHeader("SWAP_FREE",  swapfree);
event:addHeader("MEM_USAGE",  totalpct);
event:addHeader("SWAP_USAGE", swappct);
event:addHeader("FREESWITCH_CPU", fswcpupercent);
event:addHeader("FREESWITCH_MEM", fswmempercent);
event:addHeader("FREESWITCH_PID", tostring(PIDint) );


event:fire();

