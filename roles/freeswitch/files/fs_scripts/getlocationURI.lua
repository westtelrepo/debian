#!/usr/bin/lua

freeswitch.consoleLog("info","Retrieve Location v1.4\n")

-- Version 1.1 add Legacy ALI
-- Version 1.2 add Trunk
--             add Callinfo
-- Version 1.3 add special character check %%3E for encoded '>' in CallInfo
-- Version 1.4 move log line for 'first'

geolocation     = session:getVariable("sip_geolocation")
id              = session:getVariable("uuid");
BidID           = session:getVariable("sip_h_X-Bid_ID");
locationbyvalue = session:getVariable("sip_multipart");
LegacyALI       = session:getVariable("sip_h_X-Legacy-ALI-b64");
Trunk           = session:getVariable("sip_h_X-Trunk");
Callinfo        = session:getVariable("sip_call_info");

--if (BidID == nil) then
-- BidID       = session:getVariable("sip_from_user");
--end


if (geolocation == nil) and (locationbyvalue == nil) and (LegacyALI == nil) then
freeswitch.consoleLog("info","No Location Info\n")
return
end


-- fire event to event socket
local event = freeswitch.Event("CUSTOM", "myevent::message");
event:addHeader("My-Name", "LOCATION_DATA");
if (geolocation ~= nil) then
   event:addHeader("Location-URI", geolocation);
   freeswitch.consoleLog("info","LocationUri="..geolocation.."\n")
end
if (locationbyvalue ~= nil) then
   event:addHeader("Location-Data", locationbyvalue);
   freeswitch.consoleLog("info","Location by Value = "..locationbyvalue.."\n")
   
   first = string.find(locationbyvalue , 'entity=')

   if (first ~= nil) then
    freeswitch.consoleLog("crit","first = "..first.."\n")
    last = string.find(locationbyvalue , '>', first )
    splast = string.find(locationbyvalue , '%%3E', first)
     if(splast ~= nil) then 
      if(splast < last) then
       last = splast
      end
     end 
     if (last ~= nil) then
      entity = string.sub(locationbyvalue , first+8, last-2)
      freeswitch.consoleLog("info","entity = "..entity.."\n")
     end
   end





end

if (LegacyALI ~= nil) then
   event:addHeader("Legacy-ALI-Data", LegacyALI);
   freeswitch.consoleLog("info","Legacy ALI = "..LegacyALI.."\n")
end

if (Callinfo~= nil) then
   event:addHeader("variable_sip_call_info", Callinfo)
   freeswitch.consoleLog("info","sip_call_info = "..Callinfo.."\n")
   if (BidID == nil) then
    first = string.find(Callinfo, 'uid:callid:')
    if (first ~= nil) then
     last = string.find(Callinfo, '>', first )
     splast = string.find(Callinfo, '%%3E', first)
     if(splast ~= nil) then
      if(splast < last) then
       last = splast
      end
     end 
     if (last ~= nil) then
      BidID = string.sub(Callinfo, first+11, last-1)
      freeswitch.consoleLog("info","BidID = "..BidID.."\n")
     end
    end
   end
end

-- need trunk info as well

if (Trunk ~= nil) and (string.len(Trunk) == 3)  then
   data = "Trunk="..string.sub(Trunk,2)
   session:execute("set",data);
   freeswitch.consoleLog("info","Trunk = "..Trunk.."\n")
else
   Trunk = session:getVariable("sip_from_user");
   if (Trunk ~= nil) and (string.len(Trunk) == 3)  then
    data = "Trunk="..string.sub(Trunk,2)
    session:execute("set",data);
    freeswitch.consoleLog("info","Trunk = "..Trunk.."\n")
   else
    session:execute("set","Trunk=00");
    freeswitch.consoleLog("info","Trunk = 00".."\n")

   end
end

event:addHeader("Trunk", Trunk);
event:addHeader("Channel-Call-UUID", id);
event:addHeader("Bid-ID", BidID);
event:addHeader("Pidflo-entity", entity);


event:fire();

session:sleep(500);

