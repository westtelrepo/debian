Role Name
=========

Installs freeswitch.

Also installs iptables-persistent with config suitable for freeswitch ONLY. This is not currently compatible with
installing with a different firewall, or a different source of firewall rules.
